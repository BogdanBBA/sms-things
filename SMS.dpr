program SMS;

uses
  Vcl.Forms,
  Main in 'Main.pas' {FMain},
  DataTypes in 'DataTypes.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMain, FMain);
  Application.Run;
end.
