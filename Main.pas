unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, UIntList, XML.VerySimple,
  Vcl.CheckLst;

type
  TFMain = class(TForm)
    PageControl1: TPageControl;
    Button1: TButton;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    le1: TLabeledEdit;
    le2: TLabeledEdit;
    Label4: TLabel;
    Button2: TButton;
    Button3: TButton;
    Label5: TLabel;
    Button4: TButton;
    OpenDialog1: TOpenDialog;
    TabSheet3: TTabSheet;
    chl: TCheckListBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Button5: TButton;
    Button6: TButton;
    Label9: TLabel;
    Label10: TLabel;
    lb1: TListBox;
    Label11: TLabel;
    Button7: TButton;
    Button8: TButton;
    Label12: TLabel;
    lb2: TListBox;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Label13: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure chlClick(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    private
      { Private declarations }
    public
      { Public declarations }
  end;

var
  FMain: TFMain;
  inList, outList: TStringList;

implementation

{$R *.dfm}


uses DataTypes;

procedure TFMain.Button1Click(Sender: TObject);
begin
  d.Free;
  FMain.Close
end;

procedure TFMain.Button2Click(Sender: TObject);
var i: word;
begin
  if OpenDialog1.Execute then
    begin
      inList.Clear;
      for i := 0 to OpenDialog1.Files.Count - 1 do
        inList.Add(OpenDialog1.Files[i]);
      Button2.Caption := Format('Inbox (%d sel.)', [inList.Count]);
      Button4.Enabled := true
    end
end;

procedure TFMain.Button3Click(Sender: TObject);
var i: word;
begin
  if OpenDialog1.Execute then
    begin
      outList.Clear;
      for i := 0 to OpenDialog1.Files.Count - 1 do
        outList.Add(OpenDialog1.Files[i]);
      Button3.Caption := Format('Outbox (%d sel.)', [outList.Count]);
      Button4.Enabled := true
    end
end;

procedure TFMain.Button4Click(Sender: TObject);
var i: word;
begin
  d.ErrorL.Clear;
  myName := le1.Text;
  myNumber := GoodNumber(le2.Text);
  if inList.Count > 0 then
    for i := 0 to inList.Count - 1 do
      d.AddInboxSMS(inList[i]);
  if outList.Count > 0 then
    for i := 0 to outList.Count - 1 do
      d.AddSentSMS(outList[i]);
  d.SaveXML;
  d.SaveContactBook;
  d.ErrorL.SaveToFile('errors.txt');
  Button4.Caption := 'Done';
  if d.ErrorL.Count > 0 then
    MessageDlg('The process completed but there have been some errors. See "errors.txt" to see at what files they happened.', mtWarning, [mbOK], 0)
end;

procedure TFMain.Button5Click(Sender: TObject);
var i: word;
begin
  if chl.Items.Count > 0 then
    for i := 0 to chl.Items.Count - 1 do
      chl.Checked[i] := true;
  chlClick(Button5)
end;

procedure TFMain.Button6Click(Sender: TObject);
var i: word;
begin
  if chl.Items.Count > 0 then
    for i := 0 to chl.Items.Count - 1 do
      chl.Checked[i] := False;
  chlClick(Button6)
end;

procedure TFMain.Button7Click(Sender: TObject);
var i: word;
begin
  if chl.Items.Count > 0 then
    for i := 0 to chl.Items.Count - 1 do
      if chl.Checked[i] then
        lb1.Items.Add(chl.Items[i])
end;

procedure TFMain.Button9Click(Sender: TObject);
var i: word;
begin
  if chl.Items.Count > 0 then
    for i := 0 to chl.Items.Count - 1 do
      if chl.Checked[i] then
        lb2.Items.Add(chl.Items[i])
end;

procedure TFMain.Button8Click(Sender: TObject);
begin
  lb1.Items.Clear
end;

procedure TFMain.Button10Click(Sender: TObject);
begin
  lb2.Items.Clear
end;

procedure TFMain.chlClick(Sender: TObject);
var i, n: word;
begin
  n := 0;
  if chl.Items.Count > 0 then
    for i := 0 to chl.Items.Count - 1 do
      Inc(n, Integer(chl.Checked[i]));
  Label9.Caption := Format('3. Select the contacts you want (%d/%d)', [n, chl.Items.Count])
end;

procedure TFMain.FormCreate(Sender: TObject);
begin
  inList := TStringList.Create;
  outList := TStringList.Create;
  d := TData.Create;
  PageControl1.ActivePageIndex := 0
end;

procedure TFMain.TabSheet3Show(Sender: TObject);
var i: word; x: TXMLVerySimple;
begin
  chl.Items.Clear;
  if not FileExists('contacts.xml') then
    begin
      MessageDlg('File "contacts.xml" does not exist!', mtError, [mbCancel], 0);
      Exit
    end;
  x := TXMLVerySimple.Create;
  x.LoadFromFile('contacts.xml');
  for i := 0 to x.Root.ChildNodes.Count - 1 do
    chl.Items.Add(x.Root.ChildNodes[i].Attribute['name']);
  x.Free;
  Button6Click(TabSheet3);
  Button8Click(TabSheet3);
  Button10Click(TabSheet3)
end;

procedure TFMain.Button11Click(Sender: TObject);
var l1, l2: TStringList; i: word; x: TXMLVerySimple;
begin
  if (lb1.Items.Count = 0) or (lb2.Items.Count = 0) then
    begin
      MessageDlg('Contact list boxes cannot be empty!', mtError, [mbCancel], 0);
      Exit
    end;
  l1 := TStringList.Create;
  l2 := TStringList.Create;
  x := TXMLVerySimple.Create;
  x.LoadFromFile('contacts.xml');
  for i := 0 to lb1.Items.Count - 1 do
    l1.Add(x.Root.Find('contact', 'name', lb1.Items[i]).Attribute['number']);
  for i := 0 to lb2.Items.Count - 1 do
    l2.Add(x.Root.Find('contact', 'name', lb2.Items[i]).Attribute['number']);
  Button11.Caption := 'Prep complete...';
  d.ExportSelection(l1, l2);
  l1.Free;
  l2.Free;
  x.Free;
  Button11.Caption := 'Done'
end;

end.
