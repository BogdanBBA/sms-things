object FMain: TFMain
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 
    'SMS Things - by BogdyBBA - v1.0 alpha: Jul 17, 2013 / beta: Aug ' +
    '11, 2013'
  ClientHeight = 400
  ClientWidth = 508
  Color = 6701630
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  ScreenSnap = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 20
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 492
    Height = 336
    ActivePage = TabSheet3
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Convert .VMG'
      object Label1: TLabel
        Left = 16
        Top = 3
        Width = 382
        Height = 20
        Caption = '1. Make backup of phone messages on PC (.NBU file);'
      end
      object Label2: TLabel
        Left = 16
        Top = 29
        Width = 442
        Height = 20
        Caption = '2. Use the app NbuExplorer to extract messages to .VMG files;'
      end
      object Label3: TLabel
        Left = 16
        Top = 55
        Width = 125
        Height = 20
        Caption = '3. Fill in your info:'
      end
      object Label4: TLabel
        Left = 16
        Top = 130
        Width = 288
        Height = 20
        Caption = '4. Select the .VMG files to be processed:'
      end
      object Label5: TLabel
        Left = 16
        Top = 192
        Width = 378
        Height = 20
        Caption = '5. Click "Process" and if ok, proceed to the next tab..'
      end
      object le1: TLabeledEdit
        Left = 40
        Top = 96
        Width = 177
        Height = 29
        EditLabel.Width = 52
        EditLabel.Height = 18
        EditLabel.Caption = 'My name'
        EditLabel.Font.Charset = ANSI_CHARSET
        EditLabel.Font.Color = clWindowText
        EditLabel.Font.Height = -13
        EditLabel.Font.Name = 'Ubuntu'
        EditLabel.Font.Style = [fsItalic]
        EditLabel.ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Ubuntu'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        Text = 'BogdyBBA'
      end
      object le2: TLabeledEdit
        Left = 248
        Top = 96
        Width = 177
        Height = 29
        EditLabel.Width = 102
        EditLabel.Height = 18
        EditLabel.Caption = 'My phone number'
        EditLabel.Font.Charset = ANSI_CHARSET
        EditLabel.Font.Color = clWindowText
        EditLabel.Font.Height = -13
        EditLabel.Font.Name = 'Ubuntu'
        EditLabel.Font.Style = [fsItalic]
        EditLabel.ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Ubuntu'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        Text = '0769653773'
      end
      object Button2: TButton
        Left = 40
        Top = 156
        Width = 177
        Height = 30
        Caption = 'Inbox (none selected)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Ubuntu Medium'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = Button2Click
      end
      object Button3: TButton
        Left = 248
        Top = 156
        Width = 177
        Height = 30
        Caption = 'Outbox (none selected)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Ubuntu Medium'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 248
        Top = 218
        Width = 177
        Height = 30
        Caption = 'PROCESS'
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Ubuntu Medium'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = Button4Click
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Processing'
      ImageIndex = 1
    end
    object TabSheet3: TTabSheet
      Caption = 'Export'
      ImageIndex = 2
      OnShow = TabSheet3Show
      object Label6: TLabel
        Left = 3
        Top = 0
        Width = 54
        Height = 18
        Caption = 'Contacts:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Ubuntu'
        Font.Style = [fsItalic]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 175
        Top = 3
        Width = 306
        Height = 20
        Caption = '1. Edit "contacts.xml" and "data.xml" -> set'
      end
      object Label8: TLabel
        Left = 194
        Top = 23
        Width = 293
        Height = 21
        Caption = 'encoding to UTF8, then reopen this tab.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Ubuntu'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 175
        Top = 45
        Width = 226
        Height = 20
        Caption = '2. Select the contacts you want'
      end
      object Label10: TLabel
        Left = 194
        Top = 64
        Width = 266
        Height = 20
        Caption = 'and place them accordingly left/right.'
      end
      object Label11: TLabel
        Left = 175
        Top = 86
        Width = 33
        Height = 17
        Caption = '"Me":'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Ubuntu'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 322
        Top = 86
        Width = 87
        Height = 17
        Caption = '"You"/"them":'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Ubuntu'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 175
        Top = 273
        Width = 129
        Height = 20
        Caption = '3. Click "process".'
      end
      object chl: TCheckListBox
        Left = 3
        Top = 16
        Width = 166
        Height = 251
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Ubuntu'
        Font.Style = []
        Items.Strings = (
          '5645645'
          'fgdgdf'
          'gdfgdf')
        ParentFont = False
        Sorted = True
        TabOrder = 0
        OnClick = chlClick
      end
      object Button5: TButton
        Left = 3
        Top = 273
        Width = 81
        Height = 25
        Caption = 'Sel. all'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Ubuntu'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = Button5Click
      end
      object Button6: TButton
        Left = 90
        Top = 273
        Width = 79
        Height = 25
        Caption = 'Sel. none'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Ubuntu'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        OnClick = Button6Click
      end
      object lb1: TListBox
        Left = 175
        Top = 104
        Width = 138
        Height = 132
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Ubuntu'
        Font.Style = []
        Items.Strings = (
          'fsdfsd'
          'sdfsdfsd'
          '78686'
          'hgf')
        ParentFont = False
        TabOrder = 3
      end
      object Button7: TButton
        Left = 175
        Top = 242
        Width = 82
        Height = 25
        Caption = 'Add sel.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Ubuntu'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        OnClick = Button7Click
      end
      object Button8: TButton
        Left = 256
        Top = 242
        Width = 57
        Height = 25
        Caption = 'Clear'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Ubuntu'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnClick = Button8Click
      end
      object lb2: TListBox
        Left = 322
        Top = 104
        Width = 138
        Height = 132
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Ubuntu'
        Font.Style = []
        Items.Strings = (
          'fsdfsd'
          'sdfsdfsd'
          '78686'
          'hgf')
        ParentFont = False
        TabOrder = 6
      end
      object Button9: TButton
        Left = 322
        Top = 242
        Width = 82
        Height = 25
        Caption = 'Add sel.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Ubuntu'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        OnClick = Button9Click
      end
      object Button10: TButton
        Left = 403
        Top = 242
        Width = 57
        Height = 25
        Caption = 'Clear'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Ubuntu'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        OnClick = Button10Click
      end
      object Button11: TButton
        Left = 322
        Top = 268
        Width = 138
        Height = 30
        Caption = 'PROCESS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Ubuntu Medium'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        OnClick = Button11Click
      end
    end
  end
  object Button1: TButton
    Left = 360
    Top = 350
    Width = 140
    Height = 42
    Caption = 'Exit'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = Button1Click
  end
  object OpenDialog1: TOpenDialog
    Filter = 'VMG files|*.vmg'
    FilterIndex = 0
    InitialDir = 'C:\Users\BogdyBBA\Desktop\poze\Messages'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Select files'
    Left = 40
    Top = 351
  end
end
