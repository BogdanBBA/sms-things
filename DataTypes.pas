unit DataTypes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UIntList, XML.VerySimple, DateUtils;

const
	nl=#13#10; dnl=nl+nl;

type TMessage=class
	public
	  when: TDateTime;
  	fromN, toN, msg: string;
end;

type TData=class
  private
  	XML: TXMLVerySimple;
  public
  	SMS: TList;
    ErrorL: TStringList;
    procedure AddInboxSMS(const fn: string);
    procedure AddSentSMS(const fn: string);
    procedure SaveXML;
    procedure SaveContactBook;
    procedure ExportSelection(me, you: TStringList);
  	constructor Create;
    destructor Free;
end;

var
	d: TData;
  myName, myNumber: string;

function GoodNumber(nb: string): string;
function GetVMGDate(x: string): TDateTime;
function GetXMLDate(x: string): TDateTime;
function CompareMessages(p1, p2: Pointer): integer;

implementation

uses FunctiiBaza;

procedure TData.AddInboxSMS(const fn: string);
var sl: TStringList; m: TMessage; sx: string;
begin
	try
  	sl:=TStringList.Create; sl.LoadFromFile(fn, TEncoding.Unicode);
    m:=TMessage.Create; m.toN:=myNumber;
    while copy(sl[0], 1, 4)<>'TEL:' do sl.Delete(0); m.fromN:=GoodNumber(copy(sl[0], 5, 50));
    while sl[0]<>'BEGIN:VBODY' do sl.Delete(0); sl.Delete(0);
    sx:=copy(sl[0], 6, 50); m.when:=GetVMGDate(sx); sl.Delete(0); m.msg:='';
    while sl[0]<>'END:VBODY' do begin m.msg:=m.msg+sl[0]+'#nl#'; sl.Delete(0) end; delete(m.msg, length(m.msg)-3, 4);
    m.msg:=StringReplace(m.msg, '<', '#st#', [rfReplaceAll]); m.msg:=StringReplace(m.msg, '>', '#gt#', [rfReplaceAll]);
    m.msg:=StringReplace(m.msg, '"', '#gh#', [rfReplaceAll]);
    sl.Free; SMS.Add(m)
  except on E:Exception do
  	begin
	  	ErrorL.Add(fn);
  	  MessageDlg(Format('AddInboxSMS(fn="%s") ERROR:%s%s - %s%sThe SMS will not be added to the list.', [fn, dnl, E.ClassName, E.Message, dnl]), mtError, [mbIgnore], 0)
    end
  end
end;

procedure TData.AddSentSMS(const fn: string);
var sl: TStringList; m: TMessage; sx: string;
begin
	try
  	sl:=TStringList.Create; sl.LoadFromFile(fn, TEncoding.Unicode);
    m:=TMessage.Create; m.fromN:=myNumber;
    while copy(sl[0], 1, 4)<>'TEL:' do sl.Delete(0);
    if length(sl[0])<=4 then begin sl.Delete(0); while copy(sl[0], 1, 4)<>'TEL:' do sl.Delete(0) end;
    m.toN:=GoodNumber(copy(sl[0], 5, 50));
    while sl[0]<>'BEGIN:VBODY' do sl.Delete(0); sl.Delete(0);
    sx:=copy(sl[0], 6, 50); m.when:=GetVMGDate(sx); sl.Delete(0); m.msg:='';
    while sl[0]<>'END:VBODY' do begin m.msg:=m.msg+sl[0]+'#nl#'; sl.Delete(0) end; delete(m.msg, length(m.msg)-3, 4);
    m.msg:=StringReplace(m.msg, '<', '#st#', [rfReplaceAll]); m.msg:=StringReplace(m.msg, '>', '#gt#', [rfReplaceAll]);
    m.msg:=StringReplace(m.msg, '"', '#gh#', [rfReplaceAll]);
    {m.msg:=StringReplace(m.msg, '�', 'a', [rfReplaceAll]); m.msg:=StringReplace(m.msg, '�', 'A', [rfReplaceAll]);
    m.msg:=StringReplace(m.msg, '�', 'i', [rfReplaceAll]); m.msg:=StringReplace(m.msg, '�', 'I', [rfReplaceAll]);}
    sl.Free; SMS.Add(m)
  except on E:Exception do
  	begin
	  	ErrorL.Add(fn);
  	  MessageDlg(Format('AddInboxSMS(fn="%s") ERROR:%s%s - %s%sThe SMS will not be added to the list.', [fn, dnl, E.ClassName, E.Message, dnl]), mtError, [mbIgnore], 0)
    end
  end
end;

procedure TData.SaveXML;
var i: word; p: TMessage;
begin
	SMS.Sort(CompareMessages);
  XML.Clear; XML.Root.NodeName:='SMS'; XML.Root.SetAttribute('saved', FormatDateTime('yyyy.mm.dd hh:nn:ss', now));
	if SMS.Count>0 then for i:=0 to SMS.Count-1 do
  	begin
      XML.Root.AddChild('sms'); p:=TMessage(SMS[i]);
      with XML.Root.ChildNodes.Last do
      	begin
      		SetAttribute('when', FormatDateTime('yyyy/mm/dd hh:nn:ss', p.when));
      		SetAttribute('from', p.fromN);
      		SetAttribute('to', p.toN);
      		SetAttribute('msg', p.msg)
        end
    end;
  XML.SaveToFile('data.xml')
end;

procedure TData.SaveContactBook;
	{*}procedure AddIfDoesntExist(s: string);
  begin
    if XML.Root.Find('contact', 'number', s)=nil then
    	begin
        XML.Root.AddChild('contact'); with XML.Root.ChildNodes.Last do begin SetAttribute('number', s); SetAttribute('name', 'unknown') end
      end
  end;
var i, j: word; sx: string; nod: TXMLNode; l: TList;
begin
  SMS.Sort(CompareMessages);
  XML.Clear; if FileExists('contacts.xml') then XML.LoadFromFile('contacts.xml');
  XML.Root.NodeName:='ContactBook'; XML.Root.SetAttribute('saved', FormatDateTime('yyyy.mm.dd hh:nn:ss', now));
	if SMS.Count>0 then for i:=0 to SMS.Count-1 do
  	begin AddIfDoesntExist(TMessage(SMS[i]).fromN); AddIfDoesntExist(TMessage(SMS[i]).toN) end;
  if XML.Root.Find('contact', 'number', myNumber)<>nil then
  	begin nod:=XML.Root.Find('contact', 'number', myNumber); nod.Attribute['name']:=myName; XML.Root.ChildNodes.Extract(nod); XML.Root.ChildNodes.Insert(0, nod) end;
  // DO SOME CONTACT SORTING BY PHONE NUMBER
  XML.SaveToFile('contacts.xml')
end;

procedure TData.ExportSelection(me, you: TStringList);
const v1: array[1..2] of string=('left', 'right');
			v2: array[1..2] of string=('TextBubble1', 'TextBubble2');
	{*}function SMS_OK(x: TXMLNode; l1, l2: TStringList): Byte;
  var i: Word; k1, k2: boolean;
  begin
    // A -> B
    k1:=False; k2:=False;
    for i:=0 to l1.Count-1 do if l1[i]=x.Attribute['from'] then k1:=True;
    for i:=0 to l2.Count-1 do if l2[i]=x.Attribute['to'] then k2:=True;
    if k1 and k2 then begin Result:=1; Exit end;
    // B -> A
    k1:=False; k2:=False;
    for i:=0 to l2.Count-1 do if l2[i]=x.Attribute['from'] then k1:=True;
    for i:=0 to l1.Count-1 do if l1[i]=x.Attribute['to'] then k2:=True;
    if k1 and k2 then begin Result:=2; Exit end;
    // Meh
    Result:=0
  end;
var f: TextFile; aux: TXMLVerySimple; node: TXMLNode; i, k: Word;
begin
  XML.Free; XML:=TXMLVerySimple.Create; XML.LoadFromFile('data.xml');
  aux:=TXMLVerySimple.Create; aux.LoadFromFile('contacts.xml');

  AssignFile(f, FormatDateTime('"export\export "yyyy-mm-dd hh.nn.ss".html"', now)); ReWrite(f);
  WriteLn(f, '<html>');
  WriteLn(f, '  <head>');
  WriteLn(f, '    <title>SMS Thing Selection</title>');
  WriteLn(f, '      <link rel="stylesheet" type="text/css" href="style.css" /> ');
  WriteLn(f, '  </head>');
  WriteLn(f, '  <body>');
  WriteLn(f, '    <titlu>SMS Selection</titlu><br><br>');
  WriteLn(f, '    <det>Exported <date>', FormatDateTime('dddd, d mmmm yyyy, h:nn:ss', now), '</date></det><br><br>');
  WriteLn(f, '    <details><ul>');
  WriteLn(f, '        <li>Out conts: <statItem>', me.Count, '</statItem><br><det>');
  for i:=0 to me.Count-1 do
  	WriteLn(f, '[', i+1, ']. <cont>', aux.Root.Find('contact', 'number', me[i]).Attribute['name'], '</cont> <tel>(', me[i], ')</tel>; ');
  WriteLn(f, '</det></li>');
  WriteLn(f, '        <li>In conts: <statItem>', you.Count, '</statItem><br><det>');
  for i:=0 to you.Count-1 do
  	WriteLn(f, '[', i+1, ']. <cont>', aux.Root.Find('contact', 'number', you[i]).Attribute['name'], '</cont> <tel>(', you[i], ')</tel>; ');
  WriteLn(f, '</det></li>');
  WriteLn(f, '    </ul></details><br>');
  WriteLn(f, '    ');
  WriteLn(f, '    <table class="TabelNormal">');
  for i:=0 to XML.Root.ChildNodes.Count-1 do
  	begin
      node:=XML.Root.ChildNodes[i]; k:=SMS_OK(node, me, you);
      if k<>0 then
      	begin
          WriteLn(f, '      <tr> <td align="', v1[k], '">');
				  WriteLn(f, '          <det>Sent by <cont>', aux.Root.Find('contact', 'number', node.Attribute['from']).Attribute['name'], '</cont> <tel>(', node.Attribute['from'], ')</tel> to '
													            				 +'<cont>', aux.Root.Find('contact', 'number', node.Attribute['to']).Attribute['name'],   '</cont> <tel>(', node.Attribute['to'],   ')</tel> '
                                               +'on <date>', FormatDateTime('ddd, d mmmm yyyy, h:nn:ss', GetXMLDate(node.Attribute['when'])), '</date></det>');
				  WriteLn(f, '          <div class="', v2[k], '">', node.Attribute['msg'], '</div><br>');
				  WriteLn(f, '      </td> </tr>');
        end
    end;
  WriteLn(f, '    </table>');
  WriteLn(f, '    ');
  WriteLn(f, '    <br><det>HTML by <cont>BogdyBBA</cont> as of <date>August 11th, 2013</date></det><br>');
  WriteLn(f, '  </body>');
  WriteLn(f, '</html>');

  CloseFile(f); aux.Free
end;

constructor TData.Create;
begin
  XML:=TXMLVerySimple.Create; SMS:=TList.Create; ErrorL:=TStringList.Create
end;

destructor TData.Free;
begin
  if XML<>nil then XML.Free; if SMS<>nil then SMS.Free; if ErrorL<>nil then ErrorL.Free
end;

//

function GoodNumber(nb: string): string;
begin
  if nb[1]='+' then delete(nb, 1, 1);
  if nb[1]='4' then delete(nb, 1, 1);
  result:=nb
end;

function GetVMGDate(x: string): TDateTime;
var y, m, d, h, n, s: word;
begin
  try                   //showmessagefmt('x=%s%sr=%s', [x, dnl, FormatDateTime('dd mmm yyyy, h:nn:ss', result)]);
  	d:=StrToInt(copy(x, 1, 2)); m:=StrToInt(copy(x, 4, 2)); y:=StrToInt(copy(x, 7, 4));
    h:=StrToInt(copy(x, 12, 2)); n:=StrToInt(copy(x, 15, 2)); s:=StrToInt(copy(x, 18, 2));
    result:=EncodeDateTime(y, m, d, h, n, s, 0);
  except
    result:=EncodeDateTime(1900, 1, 1, 0, 0, 0, 0)
  end
end;

function GetXMLDate(x: string): TDateTime;
var y, m, d, h, n, s: word;
begin
  try
  	y:=StrToInt(copy(x, 1, 4)); m:=StrToInt(copy(x, 6, 2)); d:=StrToInt(copy(x, 9, 2));
    h:=StrToInt(copy(x, 12, 2)); n:=StrToInt(copy(x, 15, 2)); s:=StrToInt(copy(x, 18, 2));
    result:=EncodeDateTime(y, m, d, h, n, s, 0);
  except
    result:=EncodeDateTime(1900, 1, 1, 0, 0, 0, 0)
  end
end;

function CompareMessages(p1, p2: Pointer): integer;
begin
  if TMessage(p1).when>TMessage(p2).when then result:=1
  else if TMessage(p1).when<TMessage(p2).when then result:=-1
  else result:=0
end;

end.
